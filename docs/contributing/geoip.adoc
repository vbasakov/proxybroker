== geoip resolver

Common name for mechanism of resolving country code via ip address

=== maxmind
This product offers https://dev.maxmind.com/geoip/updating-databases?lang=en[binary db update] and https://maxmind.github.io/GeoIP2-java/[library] for interaction with https://dev.maxmind.com/geoip/geolite2-free-geolocation-data[GeoLite2 Country] data file.


IMPORTANT: DB is free, but you have to register as company for https://www.maxmind.com/en/accounts/current/geoip/downloads[download]
or use alternative distributions:
- https://github.com/P3TERX/GeoLite.mmdb
- https://github.com/Loyalsoldier/geoip/releases


==== self update
It based on GitHub release assets. For understanding try script

[source,bash]
----
curl -s \
  -H "Accept: application/vnd.github+json" \
  -H "X-GitHub-Api-Version: 2022-11-28" \
  https://api.github.com/repos/P3TERX/GeoLite.mmdb/releases/latest | \
  jq '{"tag_name": .tag_name, "name": .assets[2].name, "url": .assets[2].browser_download_url}'
----
and get pleasure from response:

[source,json]
----
{
  "tag_name": "2023.04.07",
  "name": "GeoLite2-Country.mmdb",
  "url": "https://api.github.com/repos/P3TERX/GeoLite.mmdb/releases/assets/102649307"
}
----

As mixmind publish updates every Tuesday and Friday, there is no need to check server more than once every 3 days. Because of that there is constant `app.geo.update.dalay`