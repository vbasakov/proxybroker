package to.molehole.proxybroker.service;

import to.molehole.proxybroker.dto.GenericDTO;
import to.molehole.proxybroker.exception.ProjectGenericAddException;
import to.molehole.proxybroker.exception.ProjectGenericDeleteException;
import to.molehole.proxybroker.mapper.GenericMapper;
import to.molehole.proxybroker.model.GenericModel;
import to.molehole.proxybroker.repository.GenericRepository;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {
    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapper<E, D> mapper;

//    @BeforeEach
//    void init() {
//        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(CustomUserDetails
//                .builder()
//                .username("USER"),
//                null,
//                null);
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//    }

    protected abstract void getAll();

    protected abstract void getOne();

    protected abstract void create() throws ProjectGenericAddException;

    protected abstract void update();

    protected abstract void delete() throws ProjectGenericDeleteException;
}

