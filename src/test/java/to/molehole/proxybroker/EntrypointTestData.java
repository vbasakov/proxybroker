package to.molehole.proxybroker;

import com.neovisionaries.i18n.CountryCode;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.model.Entrypoint;
import to.molehole.proxybroker.model.ExitNodeSelectionStrategy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public interface EntrypointTestData {

    EntrypointDTO ENTRYPOINT_DTO_1 = new EntrypointDTO("test1",
            1111,
            ConnectionType.HTTP,
            AnonymityLevel.NOA,
            ExitNodeSelectionStrategy.STABLE,
            new HashSet<>(Arrays.asList(CountryCode.GB, CountryCode.DE))
    );

    EntrypointDTO ENTRYPOINT_DTO_2 = new EntrypointDTO("test2",
            22,
            ConnectionType.HTTP,
            AnonymityLevel.ANM,
            ExitNodeSelectionStrategy.STABLE,
            new HashSet<>(Arrays.asList(CountryCode.NL, CountryCode.BA))
    );

    List<EntrypointDTO> ENTRYPOINT_DTO_LIST = Arrays.asList(ENTRYPOINT_DTO_1, ENTRYPOINT_DTO_2);

    Entrypoint ENTRYPOINT_1 = new Entrypoint("test1",
            1111,
            ConnectionType.HTTP,
            AnonymityLevel.NOA,
            ExitNodeSelectionStrategy.STABLE,
            new HashSet<>());

    Entrypoint ENTRYPOINT_2 = new Entrypoint("test2",
            22,
            ConnectionType.HTTP,
            AnonymityLevel.ANM,
            ExitNodeSelectionStrategy.STABLE,
            new HashSet<>());
    List<Entrypoint> ENTRYPOINT_LIST = Arrays.asList(ENTRYPOINT_1, ENTRYPOINT_2);
}
