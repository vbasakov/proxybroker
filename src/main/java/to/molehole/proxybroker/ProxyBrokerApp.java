package to.molehole.proxybroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProxyBrokerApp {

    public static void main(String[] args) {
        SpringApplication.run(ProxyBrokerApp.class, args);
    }

}
