package to.molehole.proxybroker.repository;

import org.springframework.stereotype.Repository;
import to.molehole.proxybroker.model.Entrypoint;

@Repository
public interface EntrypointRepository
    extends GenericRepository<Entrypoint> {
}
