package to.molehole.proxybroker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.security.core.AuthenticationException;
import to.molehole.proxybroker.constatns.Errors;

import java.nio.file.AccessDeniedException;

@RestControllerAdvice
public class ExceptionTranslator {
    @ExceptionHandler(ProjectGenericDeleteException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handleGeneralDeleteException(ProjectGenericDeleteException ex) {
        return proceedFieldsErrors(ex, Errors.REST.DELETE_ERROR_MESSAGE, ex.getMessage());
    }

    @ExceptionHandler(ProjectGenericAddException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handleGeneralAddException(ProjectGenericAddException ex) {
        return proceedFieldsErrors(ex, Errors.REST.ADD_ERROR_MESSAGE, ex.getMessage());
    }

//    @ExceptionHandler(AuthenticationException.class)
//    @ResponseStatus(HttpStatus.FORBIDDEN)
//    public ErrorDTO handleAuthException(AuthenticationException ex) {
//        return proceedFieldsErrors(ex, Errors.REST.AUTH_ERROR_MESSAGE, ex.getMessage());
//    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDTO handleAuthException(AccessDeniedException ex) {
        return proceedFieldsErrors(ex, Errors.REST.AUTH_ERROR_MESSAGE, ex.getMessage());
    }

    @ExceptionHandler(ExitnodeCheckException.class)
    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    public ErrorDTO handleExitnodeAccessException(ExitnodeCheckException ex) {
        return proceedFieldsErrors(ex, Errors.REST.EXITNODE_CHECK_ERROR_MESSAGE, ex.getMessage());
    }


    private ErrorDTO proceedFieldsErrors(Exception ex,
                                         String error,
                                         String description) {
        ErrorDTO errorDTO = new ErrorDTO(error, description);
        errorDTO.add(ex.getClass().getName(), "", errorDTO.getMessage());
        return errorDTO;
    }
}

