package to.molehole.proxybroker.dto.echo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HttpbinHeadersAtBody {
    @JsonProperty("Accept")
    private String userAgent;

    @JsonProperty("Via")
    private String via;

    @JsonProperty("X-Forwarded-For")
    private String xForwardedFor;

    @JsonProperty("X-Forwarded-Port")
    private String xForwardedPort;

    @JsonProperty("X-Forwarded-Proto")
    private String xForwardedProto;

    @JsonProperty("Proxy-Connection")
    private String proxyConnection;
}
