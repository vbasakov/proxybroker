package to.molehole.proxybroker.dto.echo;

import lombok.*;

@Getter
@Setter
@ToString
public class AZenvResponseHtmlBody {

    private String pre;
}
