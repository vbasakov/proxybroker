package to.molehole.proxybroker.dto.echo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HttpbinResponse {

    @JsonProperty("args")
    private HashMap<String, String> args;

    @JsonProperty("headers")
    private HttpbinHeadersAtBody echoHeadersAtBody;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("url")
    private String url;
}
