package to.molehole.proxybroker.dto.echo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.*;

@Getter
@Setter
@XmlRootElement(name = "html", namespace = "http://www.w3.org/1999/xhtml")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class AZenvResponse {

    private String head;

    private String body;

}

