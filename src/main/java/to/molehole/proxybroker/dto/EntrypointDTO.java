package to.molehole.proxybroker.dto;

import com.neovisionaries.i18n.CountryCode;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.model.ExitNodeSelectionStrategy;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EntrypointDTO
    extends GenericDTO{

    private String name;

    private Integer port;

    @Enumerated(EnumType.STRING)
    private ConnectionType connectionType;

    @Enumerated(EnumType.STRING)
    private AnonymityLevel anonymityLevel;

    @Enumerated(EnumType.STRING)
    private ExitNodeSelectionStrategy selectionStrategy;

    @Enumerated(EnumType.STRING)
    private Set<CountryCode> countryCodeSet;

}
