package to.molehole.proxybroker.dto.exitnode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExitnodeWithStatisticDTO
    extends ExitnodeFullDTO{

    private Integer stabilityIndex;
}
