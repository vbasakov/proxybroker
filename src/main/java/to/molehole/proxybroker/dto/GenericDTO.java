package to.molehole.proxybroker.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class GenericDTO {
    private Long id;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
