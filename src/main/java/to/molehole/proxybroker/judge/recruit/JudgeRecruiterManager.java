package to.molehole.proxybroker.judge.recruit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.judge.JudgeWorker;
import to.molehole.proxybroker.judge.echo.EchoType;
import to.molehole.proxybroker.model.ConnectionType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Slf4j
public class JudgeRecruiterManager {

    private static final String fileLocation = "src/main/resources/judge/";

    private final Map<ConnectionType, Set<JudgeWorker>> workerMapByEchoType;

    public JudgeRecruiterManager() {
        this.workerMapByEchoType = new HashMap<>();
        initAllRecruits(true);
    }

    public void initAllRecruits(Boolean isThisCallAfterAppStartup) {
        EchoType.stream().forEach(echoType -> {
            try {
                upsertWorkerGroup(JudgeRecruiterWorker.incomeCandidateProcessing(fileLocation, echoType), true);
            } catch (Exception ex) {
                log.warn(ex.getMessage());
            } finally {
                workerMapByEchoType.keySet().forEach(key -> {
                    log.info("== " + key + " judge group current content ======");
                    workerMapByEchoType.get(key).forEach(value -> log.info(String.valueOf(value)));
                    log.info("==" + key + "  judge group end ======");
                });

            }
        });
    }

    public Map<ConnectionType, Set<JudgeWorker>> getWorkerMapByEchoType() {
        return workerMapByEchoType;
    }

    public void upsertWorkerGroup(Set<JudgeWorker> judgeWorkerSet,
                                  boolean saveOld) {
        // группировка по типам соединения, но без использования специальной коллекции для псевдо-случайного доступа.
        // когда-нибудь стоит переделать, что бы не бегать по циклам несколько раз подряд.
        // но пока списки судей крайне малы, можно оставить как есть
        judgeWorkerSet.forEach(judgeWorker -> judgeWorker.getConnectionTypes()
                .forEach(connectionType -> {

                    if (workerMapByEchoType.containsKey(connectionType) && saveOld) {
                        Set<JudgeWorker> judgeWorkersSetTemp = new HashSet<>(workerMapByEchoType.get(connectionType));
                        judgeWorkersSetTemp.add(judgeWorker);
                        workerMapByEchoType.put(connectionType, judgeWorkersSetTemp);
                    } else {
                        workerMapByEchoType.put(connectionType, Set.of(judgeWorker));
                    }

                }));
    }

}
