package to.molehole.proxybroker.judge.echo;

import java.util.stream.Stream;

public enum EchoType {

    /**
     * compatible with response of httbin.org
     */
    HTTPBIN("httpbin"),
    /**
     * compatible with response of azenv.php
     */
    AZENV("azenv");

    private final String code;

    EchoType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static EchoType getByFilename(String nameWithExt) {
        return Stream.of(EchoType.values())
                .filter(e -> e.getCode().equals(nameWithExt))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static Stream<EchoType> stream() {
        return Stream.of(EchoType.values());
    }
}
