package to.molehole.proxybroker.judge.echo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import to.molehole.proxybroker.dto.echo.AZenvResponse;
import to.molehole.proxybroker.geodb.manage.GeoDbReaderHolder;
import to.molehole.proxybroker.service.ExitnodeService;

import java.util.Arrays;
import java.util.List;


@Service
@Slf4j
public class AZenvHttpClient {

    private static final String BASE_URL = "https://www.proxy-listen.de/azenv.php";

//    private final String currentWanIp;

    private final ExitnodeService exitnodeService;

    private final GeoDbReaderHolder geoDbReaderHolder;

    public AZenvHttpClient(ExitnodeService exitnodeService, GeoDbReaderHolder geoDbReaderHolder) {
        this.exitnodeService = exitnodeService;
        this.geoDbReaderHolder = geoDbReaderHolder;
        getCurrentWanIP();
//        this.currentWanIp = getCurrentWanIP();
    }

    public String getCurrentWanIP() {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.TEXT_HTML));
        headers.setContentType(MediaType.TEXT_HTML);
        HttpEntity<Void> entityRq = new HttpEntity<>(headers);

        String result = null;


        AZenvResponse azenvResponse = restTemplate.exchange(
                BASE_URL, HttpMethod.GET, entityRq, AZenvResponse.class
        ).getBody();

        if (azenvResponse != null) {
            log.info(azenvResponse.toString());
        } else {
            throw new RuntimeException("AZenvHttpClient#getCurrentIP azenv service is unavailable");
        }
        return result;
    }
}
