package to.molehole.proxybroker.judge.task;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.exception.ExitnodeCheckException;
import to.molehole.proxybroker.judge.echo.HttpbinHttpClient;

import java.time.LocalDateTime;

@Getter
@Slf4j
public class JudgeTask
        implements Runnable {

    private final Integer priority;
    private final LocalDateTime createdAt;
    private final ExitnodeFullDTO exitnodeFull;

    private final HttpbinHttpClient httpbinHttpClient;

    public JudgeTask(Integer priority,
                     ExitnodeFullDTO exitnodeFull,
                     HttpbinHttpClient httpbinHttpClient) {
        this.priority = priority;
        this.createdAt = LocalDateTime.now();
        this.exitnodeFull = exitnodeFull;
        this.httpbinHttpClient = httpbinHttpClient;
    }

//    @Override
//    public int compareTo(JudgeTask task) {
//        if (this.getPriority().equals(task.getPriority())) {
//            return this.getCreatedAt().compareTo(task.getCreatedAt());
//        } else {
//            return this.getPriority().compareTo(task.getPriority());
//        }
//    }

    @Override
    public String toString() {
        return "createdAd=" + getCreatedAt()
                + " provider= " + exitnodeFull.getProvider()
                + " priority=" + getPriority()
                + " host=" + exitnodeFull.getHost()
                + " port=" + exitnodeFull.getPort()
                ;
    }

    @Override
    public void run() {
        try {
            log.debug("triggered successfully: " + this);
            httpbinHttpClient.taskToExploreExitnodeDetailsById(exitnodeFull.getId());
        } catch (ExitnodeCheckException e) {
            throw new RuntimeException(e);
        }
    }
}
