package to.molehole.proxybroker.judge.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.config.judge.JudgeTaskQueueConfig;

@Component
public class JudgeTaskProducer {

    @Autowired
    private JudgeTaskQueueConfig judgeTaskQueueConfig;

    public void produceTask(JudgeTask judgeTask) {
        judgeTaskQueueConfig.JudgeTaskQueue().put(judgeTask);
    }
}
