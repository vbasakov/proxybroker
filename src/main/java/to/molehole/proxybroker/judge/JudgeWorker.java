package to.molehole.proxybroker.judge;

import lombok.Getter;
import lombok.Setter;
import to.molehole.proxybroker.judge.echo.EchoType;
import to.molehole.proxybroker.model.ConnectionType;

import java.util.Set;

@Setter
@Getter
public class JudgeWorker {

    /*
    * remote address
    * */
    private String url;

    /*
    * user defined weight of server
    * for random-weighted selection algorithm
    * */
    private Double weight;

    private EchoType echoType;

    Set<ConnectionType> connectionTypes;

    public JudgeWorker(String url, Double weight, EchoType echoType) {
        this.url = url;
        this.weight = weight;
        this.echoType = echoType;
    }

    public JudgeWorker(String url, Double weight, EchoType echoType, Set<ConnectionType> connectionTypes) {
        this(url, weight, echoType);
        this.connectionTypes = connectionTypes;
    }

    @Override
    public String toString() {
        return "{" +
                "url:" + url + ", " +
                "weight:" + weight + ", " +
                "echoType:" + echoType + ", " +
                "connectionTypeSet:" + connectionTypes +
                "}";
    }
}
