package to.molehole.proxybroker.model;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Objects;

@Converter(autoApply = true)
public class ConnectionTypeConverter
        implements AttributeConverter<ConnectionType, String> {


    @Override
    public String convertToDatabaseColumn(ConnectionType attribute) {
        return Objects.isNull(attribute)
                ? null
                : attribute.getCode();
    }

    @Override
    public ConnectionType convertToEntityAttribute(String dbData) {
        return Objects.isNull(dbData)
                ? null
                : ConnectionType.getByCode(dbData);
    }
}
