package to.molehole.proxybroker.model.exitnode;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Objects;

@Converter(autoApply = true)
public class ProviderTypeConverter
        implements AttributeConverter<ProviderType, String> {

    @Override
    public String convertToDatabaseColumn(ProviderType attribute) {
        return Objects.isNull(attribute)
                ? null
                : attribute.getCode();
    }

    @Override
    public ProviderType convertToEntityAttribute(String dbData) {
        return Objects.isNull(dbData)
                ? null
                : ProviderType.getByCode(dbData);
    }
}
