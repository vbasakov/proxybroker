package to.molehole.proxybroker.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ConnectionType {
    HTTP("http"),
    SOCKS("socks");

    private final String code;

    ConnectionType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static ConnectionType getByCode(String code) {
        return Stream.of(ConnectionType.values())
                .filter(e -> e.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static Map<String, String> asMap() {
        return Arrays.stream(ConnectionType.values())
                .collect(
                        Collectors.toMap(
                                k -> k.name(), v -> v.getCode(),
                                (prev, next) -> next, HashMap::new
                        )
                );
    }

}
