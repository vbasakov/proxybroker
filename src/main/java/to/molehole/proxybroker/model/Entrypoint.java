package to.molehole.proxybroker.model;

import com.neovisionaries.i18n.CountryCode;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "entrypoint")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "entrypoint_seq", allocationSize = 1)
public class Entrypoint extends GenericModel{

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "port", nullable = false)
    private Integer Port;

    @Column(name = "connection_type", nullable = false)
    private ConnectionType connectionType;

    //в одном соединении один уровень анонимности
    @Column(name = "anonymity_level", nullable = false)
    private AnonymityLevel anonymityLevel;

    @Column(name = "selection_strategy", nullable = false)
    private ExitNodeSelectionStrategy selectionStrategy;

    @ElementCollection(targetClass = CountryCode.class)
    @CollectionTable(name = "connections_countries", joinColumns = @JoinColumn(name = "entrypoint_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "country_code")
    private Set<CountryCode> countryCodeSet;

}
