package to.molehole.proxybroker.model;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Objects;


@Converter(autoApply = true)
public class AnonymityLevelConverter
        implements AttributeConverter<AnonymityLevel, String> {

    @Override
    public String convertToDatabaseColumn(AnonymityLevel attribute) {
        return Objects.isNull(attribute)
                ? null
                : attribute.getCode();
    }

    @Override
    public AnonymityLevel convertToEntityAttribute(String dbData) {
        return Objects.isNull(dbData)
                ? null
                : AnonymityLevel.getByCode(dbData);
    }
}
