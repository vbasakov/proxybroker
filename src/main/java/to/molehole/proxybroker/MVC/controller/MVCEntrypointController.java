package to.molehole.proxybroker.MVC.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.exception.ProjectGenericAddException;
import to.molehole.proxybroker.exception.ProjectGenericDeleteException;
import to.molehole.proxybroker.service.EntrypointService;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/entrypoints")
public class MVCEntrypointController {

    private final EntrypointService entrypointService;

    public MVCEntrypointController(EntrypointService entrypointService) {
        this.entrypointService = entrypointService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<EntrypointDTO> entrypointDTOList = entrypointService.listAll();
        model.addAttribute("entryPointList", entrypointDTOList);
        return "entrypoints/viewAll";
    }

    @GetMapping("add")
    public String create() {
        return "entrypoints/addEntrypoint";
    }

    @PostMapping("add")
    public String create(@ModelAttribute("entrypointForm") EntrypointDTO entrypointDTO) {
        entrypointService.create(entrypointDTO);
        return "redirect:/entrypoints";
    }

    @GetMapping("/remove/{id}")
    public String remove(@PathVariable Long id) throws ProjectGenericDeleteException {
        try {
            entrypointService.delete(id);
        } catch (Exception e) {
            log.error("MVCEntrypointController#remove(): " + e.getMessage());
            throw new ProjectGenericDeleteException("unsuccessful delete entrypoint");
        }
        return "redirect:/entrypoints";
    }

    @GetMapping("edit/{id}")
    public String edit(Model model,
                       @PathVariable Long id) {
        EntrypointDTO connectionToModify = entrypointService.getOne(id);
        model.addAttribute("entrypoint", connectionToModify);
        return "entrypoints/editEntrypoint";
    }

    @PostMapping("edit")
    public String edit(@ModelAttribute("entrypointForm") EntrypointDTO entrypointDTO) {
        entrypointService.update(entrypointDTO);
        return "redirect:/entrypoints";
    }


    @ExceptionHandler({ProjectGenericDeleteException.class,
            ProjectGenericAddException.class
//            , AccessDeniedException.class
    })
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("User request: " + req.getRequestURL() + " raised error " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/entrypoints", true);
    }
}
