package to.molehole.proxybroker.MVC.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import to.molehole.proxybroker.dto.exitnode.ExitnodeBasicDTO;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.dto.exitnode.ExitnodeWithStatisticDTO;
import to.molehole.proxybroker.exception.ExitnodeCheckException;
import to.molehole.proxybroker.exception.ProjectGenericAddException;
import to.molehole.proxybroker.judge.echo.HttpbinHttpClient;
import to.molehole.proxybroker.judge.task.JudgeFutureTask;
import to.molehole.proxybroker.judge.task.JudgeTask;
import to.molehole.proxybroker.judge.task.JudgeTaskTypePriority;
import to.molehole.proxybroker.config.judge.JudgeTaskExecutorConfig;
import to.molehole.proxybroker.model.exitnode.ProviderType;
import to.molehole.proxybroker.service.ExitnodeService;

import static to.molehole.proxybroker.utils.ExitNodeUtils.parseHostAndPort;

@Slf4j
@Controller
@RequestMapping("exitnode")
public class MVCExitnodeController {

    private final ExitnodeService exitnodeService;

    private final HttpbinHttpClient httpbinHttpClient;

    private final JudgeTaskExecutorConfig judgeTaskExecutorConfig;

    public MVCExitnodeController(ExitnodeService exitnodeService
            , HttpbinHttpClient httpbinHttpClient
            , JudgeTaskExecutorConfig judgeTaskExecutorConfig) {
        this.exitnodeService = exitnodeService;
        this.httpbinHttpClient = httpbinHttpClient;
        this.judgeTaskExecutorConfig = judgeTaskExecutorConfig;
    }

    @GetMapping("")
    public String getLastUserExitnodes(@RequestParam(value = "page", defaultValue = "1") int page,
                                       @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                       @ModelAttribute(name = "exception") final String exception,
                                       Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.DESC,
                "created_at"
//                "createdAt"
        ));
        Page<ExitnodeWithStatisticDTO> result;
        result = exitnodeService.getAllLastCreatedExitnodesWithStat(ProviderType.USER, pageRequest);
        model.addAttribute("exitNodes", result);
        model.addAttribute("exception", exception);
        return "exitnode/userChecks";
    }


    @PostMapping("/check")
    public String create(@ModelAttribute("exitNodeBasicForm") ExitnodeBasicDTO exitnodeBasicDTO) throws ExitnodeCheckException {
        ExitnodeFullDTO exitnodeFullDTO = parseHostAndPort(exitnodeBasicDTO);
        exitnodeFullDTO.setProvider(ProviderType.USER);
        Long nodeId = exitnodeService.create(exitnodeFullDTO).getId();
//        httpbinHttpService.taskToExploreExitnodeDetailsById(nodeId);
        exitnodeFullDTO.setId(nodeId);
        JudgeTask judgeTask = new JudgeTask(JudgeTaskTypePriority.USER.getPriorityValue(),
                exitnodeFullDTO,
                httpbinHttpClient);
        judgeTaskExecutorConfig.executorJudge().execute(new JudgeFutureTask(judgeTask));
        return "redirect:/exitnode";
    }

    @ExceptionHandler({ExitnodeCheckException.class,
            ProjectGenericAddException.class
//            , AccessDeniedException.class
    })
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("User request: " + req.getRequestURL() + " raised error " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/exitnode", true);
    }
}
