package to.molehole.proxybroker.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.model.Entrypoint;

import java.util.Set;

@Component
public class EntrypointMapper
        extends GenericMapper<Entrypoint, EntrypointDTO> {

    protected EntrypointMapper(ModelMapper mapper) {
        super(mapper, Entrypoint.class, EntrypointDTO.class);
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Entrypoint.class, EntrypointDTO.class);

        modelMapper.createTypeMap(EntrypointDTO.class, Entrypoint.class);
    }

    @Override
    protected void mapSpecificFields(EntrypointDTO source, Entrypoint destination) {
    }

    @Override
    protected void mapSpecificFields(Entrypoint source, EntrypointDTO destination) {
    }

    @Override
    protected Set<Long> getIds(Entrypoint entrypoint) {
        throw new UnsupportedOperationException("EntrypointMapper#getIds: Method unavailable");
    }

}
