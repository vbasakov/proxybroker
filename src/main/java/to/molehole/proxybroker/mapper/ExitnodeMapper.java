package to.molehole.proxybroker.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.model.exitnode.Exitnode;

import java.util.Set;

@Component
public class ExitnodeMapper
        extends GenericMapper<Exitnode, ExitnodeFullDTO>{


    protected ExitnodeMapper(ModelMapper mapper) {
        super(mapper, Exitnode.class, ExitnodeFullDTO.class);
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Exitnode.class, ExitnodeFullDTO.class);

        modelMapper.createTypeMap(ExitnodeFullDTO.class, Exitnode.class);
    }

    @Override
    protected void mapSpecificFields(ExitnodeFullDTO source, Exitnode destination) {

    }

    @Override
    protected void mapSpecificFields(Exitnode source, ExitnodeFullDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Exitnode entity) {
        throw new UnsupportedOperationException("ExitnodeFullMapper#getIds: Method unavailable");
    }
}
