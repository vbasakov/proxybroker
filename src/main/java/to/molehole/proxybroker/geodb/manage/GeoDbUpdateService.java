package to.molehole.proxybroker.geodb.manage;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import to.molehole.proxybroker.geodb.github.AssetInReleaseDTO;
import to.molehole.proxybroker.geodb.github.LatestReleaseDTO;
import to.molehole.proxybroker.model.Geodb;
import to.molehole.proxybroker.repository.GeodbRepository;
import to.molehole.proxybroker.utils.DateFormatter;
import to.molehole.proxybroker.utils.WebClientUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.DAYS;
import static to.molehole.proxybroker.geodb.github.Constants.ApiHeaders.*;
import static to.molehole.proxybroker.geodb.github.Constants.P3TERXDateFormats.formatTagNameToDate;

@Service
@Slf4j
@Getter
@Setter
public class GeoDbUpdateService {
    protected Integer updateDelay;

    private static final String FILE_BACKUP_SUFFIX = "_backup_";

    private static String VENDOR_URL_RELEASE = "https://api.github.com/repos/P3TERX/GeoLite.mmdb/releases/latest";

    private final GeodbRepository geodbRepository;

    public GeoDbUpdateService(GeodbRepository geodbRepository,
                              @Value("${app.geo.update.dalay}") Integer updateDelay) {
        this.geodbRepository = geodbRepository;
        this.updateDelay = updateDelay;
    }

    protected LatestReleaseDTO getLatestOnServer() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.set(PROTO_KEY, PROTO_VALUE);
        headers.set(PROTO_VERSION_KEY, PROTO_VERSION_VALUE);
        log.trace(String.valueOf(headers));

        HttpEntity<Void> entityRq = new HttpEntity<>(headers);

        LatestReleaseDTO latestRelease = restTemplate.exchange(
                VENDOR_URL_RELEASE, HttpMethod.GET, entityRq, LatestReleaseDTO.class
        ).getBody();

        if (latestRelease == null)
            log.warn("vendor release server in unavailable");

        log.trace("found last update on server: {}", latestRelease);
        return latestRelease;
    }

    protected void updateIfNeed(String dbLocation, String dbFileName) throws Exception {
        File dbFileObj = new File(dbLocation + dbFileName);
        LatestReleaseDTO updateToReleaseDTO = needUpdateToRelease(dbFileObj);
        if (updateToReleaseDTO == null)
            return;

        updateDbFileAndMeta(updateToReleaseDTO, dbFileObj);
    }


    protected LatestReleaseDTO needUpdateToRelease(File dbFile) throws Exception {
        Geodb geodbEntity = geodbRepository.findFirstByOrderByCreatedAtDesc();
        boolean isDbFileExist = dbFile.isFile();
        log.info("geodb file exist={}, current version is {}", isDbFileExist, geodbEntity);

        if ((geodbEntity != null) && (geodbEntity.getCreatedAt() != null) && isDbFileExist &&
                DAYS.between(geodbEntity.getCreatedAt(), LocalDate.now()) < updateDelay)
            return null;

        LatestReleaseDTO latestRelease = getLatestOnServer();
        if (latestRelease == null) {
            if (isDbFileExist) {
                log.warn("GeoDbUpdateService#needUpdate server unavailable. Local file will be used");
                return null;
            } else {
                throw new Exception("GeoDbUpdateService#needUpdate geodb distribution server unavailable");
            }
        }
        log.warn("GeoDbUpdateService#needUpdate new db distribution found");
        checkReleaseOutdated(latestRelease); // но все же лучше чем ничего
        return latestRelease;
    }

    protected void checkReleaseOutdated(LatestReleaseDTO latestRelease) {
        if (DAYS.between(
                formatTagNameToDate(latestRelease.getTagName()),
                LocalDate.now()
        ) > DayOfWeek.values().length) {
            log.warn("Probably provider is broken,  there is too old release on {}", latestRelease.getBrowserUrl());
        }
    }

    protected String getDownloadUrl(LatestReleaseDTO latestRelease, String fileName) {
        return latestRelease.getAssetInReleaseList().stream()
                .filter(a -> a.getName().equals(fileName))
                .map(AssetInReleaseDTO::getDownloadUrl)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("latestRelease DTO is corrupted"));
    }

    protected void updateDbFileAndMeta(LatestReleaseDTO latestRelease, File dbFile) throws IOException {
        String targetUrl = getDownloadUrl(latestRelease, dbFile.getName());
        String targetFile = dbFile.getPath();
        String backupFilePath = targetFile + FILE_BACKUP_SUFFIX + DateFormatter.formatForBackUps(LocalDateTime.now());

        if (dbFile.isFile()) {
            boolean isMoved = dbFile.renameTo(new File(backupFilePath));
            if (!isMoved) {
                throw new FileSystemException(backupFilePath);
            }
        }

        log.debug("trying to update from {}", targetUrl);
        WebClientUtils.downloadUsingNIO(targetUrl, targetFile);

        Geodb geodbForSave = new Geodb();
        geodbForSave.setVersion(latestRelease.getTagName());
        geodbForSave.setCreatedAt(LocalDate.now());
        geodbRepository.save(geodbForSave);
        log.info("geodb updated to version: {}", geodbForSave.getVersion());
    }
}
