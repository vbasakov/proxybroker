package to.molehole.proxybroker.geodb.manage;

import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class GeoDbReaderHolder {

    @Value("${app.geodb.filename}")
    private String dbFileName;

    @Value("${app.geodb.location}")
    private String dbLocation;

    @Autowired
    private GeoDbUpdateService geoDbUpdateService;

    private DatabaseReader singletonGeoCityReader;

    @PostConstruct
    public void initDbFile() throws Exception {
        geoDbUpdateService.updateIfNeed(dbLocation, dbFileName);
    }

    public DatabaseReader get() throws IOException {
        if (singletonGeoCityReader == null) {
            try {
                File database = new File(dbLocation + dbFileName);
                singletonGeoCityReader = new DatabaseReader.Builder(database).withCache(new CHMCache()).build();
            } catch (IOException e) {
                clear();
                throw new IOException("Exception thrown while fetching configured geoCityDatabase", e);
            }
        }
        return singletonGeoCityReader;
    }
    protected void clear() throws IOException {
        if (singletonGeoCityReader != null) {
            singletonGeoCityReader.close();
            this.singletonGeoCityReader = null;
        }
    }


}
