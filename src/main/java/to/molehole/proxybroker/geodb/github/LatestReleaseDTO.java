package to.molehole.proxybroker.geodb.github;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class LatestReleaseDTO {

    @JsonProperty("tag_name")
    private String tagName;

    @JsonProperty("assets")
    private List<AssetInReleaseDTO> assetInReleaseList;

    @JsonProperty("html_url")
    private String browserUrl;
}
