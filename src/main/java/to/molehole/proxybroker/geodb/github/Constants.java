package to.molehole.proxybroker.geodb.github;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public interface Constants {
    class ApiHeaders {
        public static final String PROTO_KEY = "Accept";
        public static final String PROTO_VALUE = "application/vnd.github+json";
        public static final String PROTO_VERSION_KEY = "X-GitHub-Api-Version";
        public static final String PROTO_VERSION_VALUE = "2022-11-28";
    }

    class P3TERXDateFormats {
        public static final DateTimeFormatter TAGNAME = DateTimeFormatter.ofPattern("yyyy.MM.dd");

        public static LocalDate formatTagNameToDate(final String dateToParse) {
            return LocalDate.parse(dateToParse, TAGNAME);
        }
    }
}
